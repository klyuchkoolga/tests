package rozetka.tests;

import com.codeborne.selenide.*;
import org.openqa.selenium.By;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Test;

import static com.codeborne.selenide.CollectionCondition.texts;
import static com.codeborne.selenide.Condition.*;
import static com.codeborne.selenide.Selectors.byText;
import static com.codeborne.selenide.Selenide.*;
import com.codeborne.selenide.Configuration;

public class MyTests {
  private By resultsBikeXpathBy =By.xpath ("//span[@class='goods-tile__title'][contains(text(),'велосипед' )]");
  private By searchAistXpathBy =By.xpath ("//h1[@class='catalog-heading']");
  private By deleteSearchXpathBy =By.xpath ("//div[@class='header-bottomline']/a");
  private By leftSideMenuXpathBy =By.xpath ("//aside//ul[contains(@class,'menu-categories_type_main')]//li/a");
  private By menuXpathBy =By.xpath ("//div/ul[@class='menu-categories']/li");
  private ElementsCollection categoryLinks;
  private ElementsCollection samsungfiltered;
  private By samsungCheckboxBy = By.xpath ("//label[contains(text(),'Samsung')]/preceding-sibling::input");
  private By samsungLinkBy = By.xpath ("//label[contains(text(),'Samsung')]");
  private By filtereSamsungBy = By.xpath ("//*[contains(@class,'catalog-grid__cell')]//span[@class='goods-tile__title']");
  private By korzinaBy = By.xpath ("//div[@class='js-rz-cart']");
  private By vashaKorzina = By.xpath("//div[@class='header-actions__dummy']//*[contains(text(),' Добавляйте понравившиеся товары в корзину ')] ");
  private By changeCity = By.cssSelector("#city-select");
  private By shop = By.xpath("//li[@class='main-addresses__item'][1]");
  private By language = By.xpath("//a[contains(text(),'UA')]");

    @BeforeClass
    public void beforeClass(){
        Configuration.startMaximized=true;
        Configuration.browser="chrome";
    }

    @BeforeMethod
    public void beforeTest(){
        open("http://rozetka.com.ua");
    }

    @Test
    public void SearchRozetka() {

        categoryLinks = $$(leftSideMenuXpathBy);

        $(By.name("search")).setValue("велосипед аист").pressEnter();

        $(searchAistXpathBy).shouldHave(text("Велосипед аист"));
        $$(resultsBikeXpathBy).shouldHave(texts("велосипед",
                "велосипед"));
        $(deleteSearchXpathBy).click();
        categoryLinks.shouldHaveSize(16);
    }


   @Test
    public void FilterRozetka()  {
       SelenideElement filter = $(byText("Каталог товаров"));
       filter.click();
       $(byText("Уцененные товары")).click();
       $(samsungLinkBy).scrollTo();

       $(samsungLinkBy).click ();
       $(samsungCheckboxBy).shouldBe (selected);
       samsungfiltered =$$(filtereSamsungBy);
       samsungfiltered.forEach(item -> {item.shouldHave (Condition.text ("Samsung"));
           System.out.println (item.text ());

       });

    }

    @Test
    public void hoverKorzina() {
    $(korzinaBy).hover();

    $(vashaKorzina).shouldBe(visible);
    }

    @Test
    public void changeCity() {
        $(changeCity).selectOption("Одессе");
        $(shop).shouldHave(text("ул. Академика Сахарова, 1б"));
    }

    @Test
   public void changeLang() {
        $(language).click();
        $(changeCity).shouldHave(text("Києві"));
    }



}

